package nl.saxion.webapps.cinemamanager.Services;

import nl.saxion.webapps.cinemamanager.Entities.Movie;
import nl.saxion.webapps.cinemamanager.Entities.Theatre;
import nl.saxion.webapps.cinemamanager.Exceptions.MovieNotFoundException;
import nl.saxion.webapps.cinemamanager.Repositories.MovieRepository;
import nl.saxion.webapps.cinemamanager.Repositories.TheatreRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by Thijs van der Vegt (497215)
 * Service for the Theatre Entity
 * Extends the BaseService
 */
@Service
public class TheatreService extends BaseService<Theatre> {
    private final MovieRepository movieRepository;

    /**
     * Constructor for the TheatreService, uses the TheatreRepository
     * @param theatreRepository -> TheatreRepository that the TheatreService uses
     */
    public TheatreService(TheatreRepository theatreRepository, MovieRepository movieRepository) {
        this.repository = theatreRepository;
        this.movieRepository = movieRepository;
    }

    /**
     * Function to get all theatres based on a location
     * @param location -> Location that you want to search for
     * @return -> List with theatres from the given location
     */
    public List<Theatre> getAllByLocation(String location) {
        return ((TheatreRepository)this.repository).findAllByLocation(location);
    }

    /**
     * Function to add a movie to a theatre
     * @param theatre -> The theatre you want to add the movie to
     * @param movie -> The movie that you want to add to the theatre
     * @return -> The added movie
     */
    public Movie addOneMovieToTheatre(Theatre theatre, Movie movie) {
        Optional<Movie> response = movieRepository.findById(movie.getId());
        if(response.isEmpty()) {
            throw new MovieNotFoundException();
        } else {
            theatre.addMovie(movie);
            this.repository.save(theatre);
            return movie;
        }
    }
}
