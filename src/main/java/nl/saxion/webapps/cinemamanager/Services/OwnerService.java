package nl.saxion.webapps.cinemamanager.Services;

import nl.saxion.webapps.cinemamanager.Entities.Owner;
import nl.saxion.webapps.cinemamanager.Repositories.OwnerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Thijs van der Vegt (497215)
 * Service for the Owner Entity
 * Extends the BaseService
 */
@Service
public class OwnerService extends BaseService<Owner> {

    /**
     * Constructor for the OwnerService, uses the OwnerRepository
     * @param ownerRepository -> OwnerRepository that the OwnerService uses
     */
    public OwnerService(OwnerRepository ownerRepository) {
        this.repository = ownerRepository;
    }

    /**
     * Function to get all the owners
     * @return -> List with all the owners
     */
    public List<Owner> getAll() {
        return this.repository.findAll();
    }
}
