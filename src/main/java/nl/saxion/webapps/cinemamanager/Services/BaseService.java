package nl.saxion.webapps.cinemamanager.Services;

import nl.saxion.webapps.cinemamanager.Repositories.BaseRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Thijs van der Vegt (497215)
 * Base Service, base for every other Service
 * @param <Type> Type of the Entity the Service is used for, e.g. Owner -> Type = Owner
 */
public abstract class BaseService<Type> {

    protected BaseRepository<Type> repository;

    /**
     * Function to 'create' a new entity, and to save it to the database
     * @param toCreate -> Entity to be created
     * @return -> The created entity
     */
    public Type create(Type toCreate) {
        return this.repository.save(toCreate);
    }

    /**
     * Function to get all the entities out of the database. Uses the 'findAll()' function of the repository
     * @return List with all entities
     */
    public List<Type> getAll() {
        return this.repository.findAll();
    }

    /**
     * Function to get an entity based on an id. Uses the 'findById()' function of the repository
     * @param id -> The id of the entity you want to get
     * @return -> The entity with the given id
     */
    public Optional<Type> getOneById(Long id) {
        return this.repository.findById(id);
    }

    /**
     * Function to 'update' an entity, and to save it to the database.
     * @param toUpdate -> The to be updated entity
     * @return -> The updated entity
     */
    public Type update(Type toUpdate) {
        return this.repository.save(toUpdate);
    }

    /**
     * Function to delete an entity based on an id from the database.
     * @param id -> Id of the entity you want to delete
     * @return True if deleted | False if failed -> (entity with given id, doesn't exist)
     */
    public boolean delete(Long id) {
        Optional<Type> item = this.repository.findById(id);
        if (item.isPresent()) {
            this.repository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
