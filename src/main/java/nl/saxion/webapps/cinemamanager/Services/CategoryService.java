package nl.saxion.webapps.cinemamanager.Services;

import nl.saxion.webapps.cinemamanager.Entities.Category;
import nl.saxion.webapps.cinemamanager.Repositories.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Thijs van der Vegt (497215)
 * Service for the Category Entity
 * Extends the BaseService
 */
@Service
public class CategoryService extends BaseService<Category> {

    /**
     * Constructor for the CategoryService, uses the CategoryRepository
     * @param categoryRepository -> CategoryRepository that the CategoryService uses
     */
    public CategoryService(CategoryRepository categoryRepository) {
        this.repository = categoryRepository;
    }

    /**
     * Function to get all the categories
     * @return -> List with all the categories
     */
    public List<Category> getAll() {
        return this.repository.findAll();
    }
}
