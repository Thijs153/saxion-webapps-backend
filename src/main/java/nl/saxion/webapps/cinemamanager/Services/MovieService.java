package nl.saxion.webapps.cinemamanager.Services;

import nl.saxion.webapps.cinemamanager.Entities.Actor;
import nl.saxion.webapps.cinemamanager.Entities.Movie;
import nl.saxion.webapps.cinemamanager.Entities.Theatre;
import nl.saxion.webapps.cinemamanager.Exceptions.ActorNotFoundException;
import nl.saxion.webapps.cinemamanager.Repositories.ActorRepository;
import nl.saxion.webapps.cinemamanager.Repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Created by Thijs van der Vegt (497215)
 * Service for the Movie Entity
 * Extends the BaseService
 * Also uses the TheatreService
 */
@Service
public class MovieService extends BaseService<Movie> {
    private final TheatreService theatreService;
    private final ActorRepository actorRepository;

    /**
     * Constructor for the MovieService, uses the MovieRepository, and the TheatreService
     * @param theatreService -> TheatreService that the MovieService uses
     * @param movieRepository -> MovieRepository that the MovieService uses
     * @param actorRepository -> ActorRepository that the MovieService uses
     */
    public MovieService(TheatreService theatreService, MovieRepository movieRepository, ActorRepository actorRepository) {
        this.repository = movieRepository;
        this.theatreService = theatreService;
        this.actorRepository = actorRepository;
    }

    /**
     * Function to get all movies based on a title
     * @param title -> Title that you want to search for
     * @return -> 'List' with movies with the given title
     */
    public List<Movie> getAllByTitle(String title) {
        return ((MovieRepository)this.repository).findAllByTitle(title);
    }

    /**
     * Function to delete a movie based on an id from the database
     * Also deletes that movie from all the theatres
     * @param id -> Id of the movie you want to delete
     * @return -> True if deleted | False if failed -> (Movie with the given id doesn't exist)
     */
    public boolean delete(Long id) {
        Optional<Movie> item = this.repository.findById(id);

        if(item.isEmpty()) {
            return false;
        } else {
            Movie movie = item.get();
            List<Theatre> allTheatres = this.theatreService.getAll();

            for (Theatre theatre : allTheatres) {
                theatre.getMovies().remove(movie);
            }

            this.repository.delete(movie);
            return true;
        }
    }

    /**
     * Function to add an actor to a movie
     * @param movie -> The movie you want to add the actor to
     * @param actor -> The actor that you want tot add tot the movie
     * @return -> The added actor
     */
    public Actor addOneActorToMovie(Movie movie, Actor actor) {
        Optional<Actor> response = actorRepository.findById(actor.getId());
        if(response.isEmpty()) {
            throw new ActorNotFoundException();
        } else {
            movie.addActor(actor);
            this.repository.save(movie);
            return actor;
        }
    }

}
