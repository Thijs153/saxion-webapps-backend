package nl.saxion.webapps.cinemamanager.Services;

import nl.saxion.webapps.cinemamanager.Entities.Actor;
import nl.saxion.webapps.cinemamanager.Entities.Movie;
import nl.saxion.webapps.cinemamanager.Repositories.ActorRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by Thijs van der Vegt (497215)
 * Service for the Actor Entity
 * Extends the BaseService
 * Also uses the MovieService
 */
@Service
public class ActorService extends BaseService<Actor> {
    private final MovieService movieService;

    /**
     * Constructor for the ActorService, uses the ActorRepository, and the MovieService
     * @param movieService -> MovieService that the ActorService uses
     * @param actorRepository -> ActorRepository that the ActorService uses
     */
    public ActorService(MovieService movieService, ActorRepository actorRepository) {
        this.repository = actorRepository;
        this.movieService = movieService;
    }

    /**
     * Function to get all actors based on a first name
     * @param firstName -> The first name you want to search for
     * @return -> List with actors with the given first name
     */
    public List<Actor> getAllByFirstName(String firstName) {
        return ((ActorRepository)this.repository).findActorsByFirstName(firstName);
    }

    /**
     * Function to get all actors based on a last name
     * @param lastName -> The last name you want to search for
     * @return -> List with actors with the given last name
     */
    public List<Actor> getAllByLastName(String lastName) {
        return ((ActorRepository)this.repository).findActorsByLastName(lastName);
    }

    /**
     * Function to delete an actor from the database
     * Also deletes that actor from all the movies
     * @param id -> Id of the actor you want to delete
     * @return True if deleted || False if failed -> (actor with the given id, doesn't exist)
     */
    public boolean delete(Long id) {
        Optional<Actor> item = this.repository.findById(id);

        if(item.isEmpty()) {
            return false;
        } else {
            Actor actor = item.get();
            List<Movie> allMovies = this.movieService.getAll();

            for(Movie movie : allMovies) {
                movie.getActors().remove(actor);
            }

            this.repository.delete(actor);
            return true;
        }
    }

    /**
     * Function to get all actors based on a first name and a last name
     * @param firstName -> First name that you want to search for
     * @param lastName -> Last name that you want to search for
     * @return -> List with actors with the given first name AND last name
     */
    public List<Actor> getAllByFirstNameAndLastName(String firstName, String lastName) {
        return ((ActorRepository)this.repository).findActorByFirstNameAndLastName(firstName, lastName);
    }
}
