package nl.saxion.webapps.cinemamanager.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Thijs van der Vegt (497215)
 * This exception will be thrown whenever a movie can't be found.
 */
@ResponseStatus(
        code = HttpStatus.NOT_FOUND,
        reason = "Movie not found!"
)
public class MovieNotFoundException extends RuntimeException {
    public MovieNotFoundException() {
    }
}
