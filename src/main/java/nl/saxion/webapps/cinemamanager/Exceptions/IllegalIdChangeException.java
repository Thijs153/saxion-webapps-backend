package nl.saxion.webapps.cinemamanager.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Thijs van der Vegt (497215)
 * This exception will be thrown whenever the id's don't match
 * (i.e., updating something (PutMapping) and giving a different id, than the id of the 'toUpdate' object).
 */
@ResponseStatus(
        code = HttpStatus.BAD_REQUEST,
        reason = "ID may not be changed!"
)
public class IllegalIdChangeException extends RuntimeException {
    public IllegalIdChangeException() {
    }
}
