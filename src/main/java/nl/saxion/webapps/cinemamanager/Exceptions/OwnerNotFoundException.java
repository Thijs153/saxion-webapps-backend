package nl.saxion.webapps.cinemamanager.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Thijs van der Vegt (497215)
 * This exception will be thrown whenever an owner can't be found.
 */
@ResponseStatus(
        code = HttpStatus.NOT_FOUND,
        reason = "Owner not found"
)
public class OwnerNotFoundException extends RuntimeException{
    public OwnerNotFoundException() {

    }
}
