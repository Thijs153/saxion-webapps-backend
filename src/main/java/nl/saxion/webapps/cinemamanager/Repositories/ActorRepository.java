package nl.saxion.webapps.cinemamanager.Repositories;


import nl.saxion.webapps.cinemamanager.Entities.Actor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Thijs van der Vegt (497215)
 * Repository for the Actor Entity
 * Extends the Base Repository
 */
@Repository
public interface ActorRepository extends BaseRepository<Actor> {

    /**
     * Find all actors based on a first name
     * @param firstName -> The first name you want to search for
     * @return -> List with actors with the given first name
     */
    List<Actor> findActorsByFirstName(String firstName);

    /**
     * Find all actors based on a last name
     * @param lastName -> The last name you want to search for
     * @return -> List with actors with the give last name
     */
    List<Actor> findActorsByLastName(String lastName);

    /**
     * Find all actors based on a first name and last name
     * @param firstName -> The first name you want to search for
     * @param lasName -> The last name you want to search for
     * @return -> List with actors with the given first and last name
     */
    List<Actor> findActorByFirstNameAndLastName(String firstName, String lasName);


}
