package nl.saxion.webapps.cinemamanager.Repositories;

import nl.saxion.webapps.cinemamanager.Entities.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Thijs van der Vegt (497215)
 * Repository for the Owner Entity
 * Extends the Base Repository
 */
@Repository
public interface OwnerRepository extends BaseRepository<Owner> {

    /* No specific functions used */

}
