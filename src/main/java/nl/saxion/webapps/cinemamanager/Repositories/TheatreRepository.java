package nl.saxion.webapps.cinemamanager.Repositories;

import nl.saxion.webapps.cinemamanager.Entities.Theatre;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Thijs van der Vegt (497215)
 * Repository for the Theatre Entity
 * Extends the Base Repository
 */
@Repository
public interface TheatreRepository extends BaseRepository<Theatre> {

    /**
     * Find all theatres based on a location
     * @param location -> The location you want to search for
     * @return -> List with theatres in the given location
     */
    List<Theatre> findAllByLocation(String location);
}
