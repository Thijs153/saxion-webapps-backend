package nl.saxion.webapps.cinemamanager.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * Created by Thijs van der Vegt (497215)
 * Base Repository, base for every other Repository.
 * @param <Type> Type of the Entity the Repository is used for, e.g. Owner -> Type = Owner
 */
@NoRepositoryBean
public interface BaseRepository<Type> extends CrudRepository<Type, Long> {

    /**
     * Find all instances of the Entity
     * @return -> List with all instances
     *
     * Overrides the findAll() method, and gives back a List instead of an Iterable
     */
    @Override
    List<Type> findAll();
}
