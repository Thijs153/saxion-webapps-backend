package nl.saxion.webapps.cinemamanager.Repositories;

import nl.saxion.webapps.cinemamanager.Entities.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Thijs van der Vegt (497215)
 * Repository for the Category Entity
 * Extends the Base Repository
 */
@Repository
public interface CategoryRepository extends BaseRepository<Category> {

    /* No specific functions used */

}
