package nl.saxion.webapps.cinemamanager.Repositories;

import nl.saxion.webapps.cinemamanager.Entities.Movie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Thijs van der Vegt (497215)
 * Repository for the Movie Entity
 * Extends the Base Repository
 */
@Repository
public interface MovieRepository extends BaseRepository<Movie> {

    /**
     * Find all movies based on a title
     * @param title -> The title you want to search for
     * @return -> List with movies with the given title
     */
    List<Movie> findAllByTitle(String title);

    /**
     * Find all movies based on a rating
     * @param rating -> The rating you want to search for
     * @return -> List with movies with the given rating
     */
    List<Movie> findAllByRating(int rating);

    /**
     * Find all movies based on the duration
     * @param lengthInMinutes -> The duration you want to search for
     * @return -> List with movies with the given duration
     */
    List<Movie> findAllByLengthInMinutes(int lengthInMinutes);


}
