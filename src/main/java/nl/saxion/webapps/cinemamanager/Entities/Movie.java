package nl.saxion.webapps.cinemamanager.Entities;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Thijs van der Vegt (497215)
 * Movie Entity
 */
@Entity
public class Movie {
    /**
     * Movie:
     * - Movie id (Long : id), unique identifier of a movie. Auto increments with every new movie
     * - Title (String : title), title of a movie
     * - Description (String : description), description of a movie
     * - Rating (Integer : rating), rating of a movie
     * - Duration (Integer : lengthInMinutes), duration of a movie in minutes
     * - Category (Category : category, ManyToOne), category of a movie, Many movies could have the same category -> ManyToOne
     * - Actors (List : actors, OneToMany), actors of a movie. One movie has many actors -> OneToMany
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private Integer rating;

    @Column
    private Integer lengthInMinutes;

    @ManyToOne
    private Category category;

    @ManyToMany(
            cascade = CascadeType.REMOVE
    )
    private List<Actor> actors;

    /* Getters and Setters */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getLengthInMinutes() {
        return lengthInMinutes;
    }

    public void setLengthInMinutes(Integer lengthInMinutes) {
        this.lengthInMinutes = lengthInMinutes;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public void addActor(Actor actor) {
        if(this.actors.contains(actor)) {
            throw new IllegalArgumentException("Actor already present");
        } else {
            this.actors.add(actor);
        }
    }
}
