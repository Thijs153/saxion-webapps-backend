package nl.saxion.webapps.cinemamanager.Entities;

import javax.persistence.*;

/**
 * Created by Thijs van der Vegt (497215)
 */
@Entity
public class Actor {
    /**
     * Actor:
     * - Actor id (Long : id), unique identifier of an actor. Auto increments with every new actor
     * - First name (String : firstName), first name of the actor
     * - Last name (String : lastName), last name of the actor
     * - Age (int : age), age of the actor
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private int age;

    /* Getters and Setters */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
