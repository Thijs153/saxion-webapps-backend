package nl.saxion.webapps.cinemamanager.Entities;

import javax.persistence.*;

/**
 * Created by Thijs van der Vegt (497215)
 * Category Entity
 */
@Entity
public class Category {
    /**
     * Category:
     * - Category id (Long : id), unique identifier of a Category. Auto increments with every new category
     * - Category Name (String : categoryName), name of the category, e.g. 'Horror'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String categoryName;

    /* Getters and Setters */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
