package nl.saxion.webapps.cinemamanager.Entities;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Thijs van der Vegt (497215)
 * Theatre Entity
 */
@Entity
public class Theatre {
    /**
     * Theatre:
     * - Theatre id (Long : id), unique identifier of a theatre. Auto increments with every new theatre
     * - Theatre name (String : theatreName), the name of a theatre
     * - Location (String : location), the location of the theatre (e.g. city name)
     * - Owner (Owner  : owner, OneToOne), the owner of the theatre -> Owner object, One theatre has one owner -> OneToOne relation.
     * - Movies (List : movies, OneToMany), list with the movies of a theatre. One theatre has many movies -> OneToMany
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String theatreName;

    @Column
    private String location;

    @OneToOne
    private Owner owner;

    @ManyToMany(
            cascade = CascadeType.REMOVE
    )
    private List<Movie> movies;

    /* Getters and Setters */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTheatreName() {
        return theatreName;
    }

    public void setTheatreName(String theatreName) {
        this.theatreName = theatreName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String address) {
        this.location = address;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwners(Owner owner) {
        this.owner = owner;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public void addMovie(Movie movie) {
        if(this.movies.contains(movie)) {
            throw new IllegalArgumentException("Movie already present");
        } else {
            this.movies.add(movie);
        }
    }
}
