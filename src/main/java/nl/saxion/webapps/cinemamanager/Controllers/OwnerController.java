package nl.saxion.webapps.cinemamanager.Controllers;

import nl.saxion.webapps.cinemamanager.Entities.Movie;
import nl.saxion.webapps.cinemamanager.Entities.Owner;
import nl.saxion.webapps.cinemamanager.Exceptions.IllegalIdChangeException;
import nl.saxion.webapps.cinemamanager.Exceptions.MovieNotFoundException;
import nl.saxion.webapps.cinemamanager.Exceptions.OwnerNotFoundException;
import nl.saxion.webapps.cinemamanager.Services.OwnerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by Thijs van der Vegt (497215)
 * Controller for the Owner Entity
 */
@RestController
@RequestMapping("/owners")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OwnerController {

    private final OwnerService ownerService;

    /**
     * Constructor for OwnerController. The Service is needed to handle requests.
     *
     * @param ownerService Spring will use dependency injection to "inject" the ownerService automatically.
     */
    public OwnerController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    /**
     * Get Function to get all the owners
     * @return List with all the owners
     */
    @GetMapping
    public List<Owner> getAllOwners() {
        return this.ownerService.getAll();
    }

    /**
     * Get Function to get a owner based on an id
     * Throws a 'OwnerNotFoundException' when the owner with the given id doesn't exist
     * @param id -> The id that you want to search for
     * @return -> The owner with the given id
     */
    @GetMapping("/{id}")
    public Owner getOneOwner(@PathVariable Long id) {
        Optional<Owner> response = this.ownerService.getOneById(id);
        if(response.isEmpty()) {
            throw new OwnerNotFoundException();
        } else {
            return response.get();
        }
    }

    /**
     * Put Function to update a owner
     * Throws an 'IllegalIdChangeException' when you try to change the id of the owner
     * @param id -> The id of the owner that you want to update
     * @param owner -> Owner object that contains the entire object including the changes
     * @return -> The updated owner
     */
    @PutMapping("/{id}")
    public Owner updateOneOwner(@PathVariable Long id, @RequestBody Owner owner) {
        Optional<Owner> response = this.ownerService.getOneById(id);
        if (response.isEmpty()) {
            throw new OwnerNotFoundException();
        } else if(!id.equals(owner.getId())) {
            throw new IllegalIdChangeException();
        } else {
            return this.ownerService.update(owner);
        }
    }

    /**
     * Post Function to create a new owner
     * Throws an 'IllegalIdChangeException' when you provide an id
     * @param owner -> The owner that you want to create
     * @return -> The created owner
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Owner addOneOwner(@RequestBody Owner owner) {
        if (owner.getId() == null) {
            return this.ownerService.create(owner);
        }
        throw new IllegalIdChangeException();
    }

    /**
     * Delete Function to delete an owner based on an id from the database
     * Throws an 'OwnerNotFoundException' when the owner with the given id doesn't exist
     * @param id -> The id of the owner that you want to delete
     */
    @DeleteMapping("/{id}")
    public void deleteOneOwner(@PathVariable Long id) {
        if(!this.ownerService.delete(id)) {
            throw new OwnerNotFoundException();
        }
    }
}
