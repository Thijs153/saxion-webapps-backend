package nl.saxion.webapps.cinemamanager.Controllers;

import nl.saxion.webapps.cinemamanager.Entities.Movie;
import nl.saxion.webapps.cinemamanager.Entities.Theatre;
import nl.saxion.webapps.cinemamanager.Exceptions.IllegalIdChangeException;
import nl.saxion.webapps.cinemamanager.Exceptions.TheatreNotFoundException;
import nl.saxion.webapps.cinemamanager.Services.TheatreService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by Thijs van der Vegt (497215)
 * Controller for the Theatre Entity
 */
@RestController
@RequestMapping("/theatres")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TheatreController {

    private final TheatreService theatreService;

    /**
     * Constructor for TheatreController. The Service is needed to handle requests.
     *
     * @param theatreService Spring will use dependency injection to "inject" the TheatreService automatically.
     */
    public TheatreController(TheatreService theatreService) {
        this.theatreService = theatreService;
    }

    /**
     * Get Function to get all the theatres (optional : based on a location)
     * @param location -> if given : the location that you want to search for
     * @return -> All theatres || All theatres from the given location
     */
    @GetMapping
    public List<Theatre> getAllTheatres(@RequestParam(required = false) String location) {
        return location != null ? this.theatreService.getAllByLocation(location) : this.theatreService.getAll();
    }

    /**
     * Get Function to get a theatre based on an id
     * Throws an 'TheatreNotFoundException' when the theatre with the given id doesn't exist
     * @param id -> The id that you want to search for
     * @return -> The theatre with the given id
     */
    @GetMapping("/{id}")
    public Theatre getOneTheatre(@PathVariable Long id) {
        Optional<Theatre> response = this.theatreService.getOneById(id);
        if(response.isEmpty()) {
            throw new TheatreNotFoundException();
        } else {
            return response.get();
        }
    }

    /**
     * Get Function to get all the movies from a theatre
     * Throws an 'TheatreNotFoundException' when the theatre with the given id doesn't exist
     * @param id -> The id of the theatre that you want to get all the movies of
     * @return -> List with all the movies from the theatre with the given id
     */
    @GetMapping("/{id}/movies")
    public List<Movie> getMoviesFromTheatre(@PathVariable Long id) {
        Optional<Theatre> response = this.theatreService.getOneById(id);
        if(response.isEmpty()) {
            throw new TheatreNotFoundException();
        } else {
            return response.get().getMovies();
        }
    }

    /**
     * Post Function to add a movie to a theatre
     * Throws an 'TheatreNotFoundException' when the theatre with the given id doesn't exist
     * @param id -> The id of the theatre
     * @param movie -> The movie that you want to add to the theatre
     * @return -> The added movie
     */
    @PostMapping("/{id}/movies")
    public Movie addMovieToTheatre(@PathVariable Long id, @RequestBody Movie movie) {
        Optional<Theatre> response = this.theatreService.getOneById(id);
        if(response.isEmpty()) {
            throw new TheatreNotFoundException();
        }
        return this.theatreService.addOneMovieToTheatre(response.get(), movie);
    }

    /**
     * Put Function to update a theatre
     * Throws an 'IllegalIdChangeException' when you try to change the id of the theatre
     * @param id -> The id of the theatre that you want to update
     * @param theatre -> Theatre object that contains the entire object including the changes
     * @return -> The updated theatre
     */
    @PutMapping("/{id}")
    public Theatre updateOneTheatre(@PathVariable Long id, @RequestBody Theatre theatre) {
        Optional<Theatre> response = this.theatreService.getOneById(id);
        if (response.isEmpty()) {
            throw new TheatreNotFoundException();
        } else if(!id.equals(theatre.getId())) {
            throw new IllegalIdChangeException();
        } else {
            return this.theatreService.update(theatre);
        }
    }

    /**
     * Post Function to create a new theatre
     * Throws an 'IllegalIdChangeException' when you provide an id.
     * @param theatre -> Theatre that you want to create (don't provide an id)
     * @return -> The created theatre
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Theatre addOneTheatre(@RequestBody Theatre theatre) {
        if (theatre.getId() == null) {
            return this.theatreService.create(theatre);
        }
        throw new IllegalIdChangeException();
    }

    /**
     * Delete Function to delete a theatre
     * Throws an 'TheatreNotFoundException' when the theatre with the given id doesn't exist
     * @param id -> The id of the theatre that you want to delete
     */
    @DeleteMapping("/{id}")
    public void deleteOnTheatre(@PathVariable Long id) {
        if(!this.theatreService.delete(id)) {
            throw new TheatreNotFoundException();
        }
    }
}
