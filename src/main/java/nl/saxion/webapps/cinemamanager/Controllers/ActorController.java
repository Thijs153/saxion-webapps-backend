package nl.saxion.webapps.cinemamanager.Controllers;

import nl.saxion.webapps.cinemamanager.Entities.Actor;
import nl.saxion.webapps.cinemamanager.Entities.Movie;
import nl.saxion.webapps.cinemamanager.Exceptions.ActorNotFoundException;
import nl.saxion.webapps.cinemamanager.Exceptions.IllegalIdChangeException;
import nl.saxion.webapps.cinemamanager.Exceptions.MovieNotFoundException;
import nl.saxion.webapps.cinemamanager.Services.ActorService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by Thijs van der Vegt (497215)
 * Controller for the Actor Entity
 */
@RestController
@RequestMapping("/actors")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ActorController {

    private final ActorService actorService;

    /**
     * Constructor for ActorController. The Service is needed to handle requests.
     *
     * @param actorService Spring will use dependency injection to "inject" the actorService automatically.
     */
    public ActorController(ActorService actorService) {
        this.actorService = actorService;
    }

    /**
     * Get Function to get all the actors (Optional, based on a first name OR last name, or based on a first name AND lastname)
     * @param firstName -> The first name that you want to search for
     * @param lastName -> The last name that you want to search for
     * @return -> List with actors with the given first name OR the given last name || List with actors with the given first name AND the given last name
     */
    @GetMapping
    public List<Actor> getAllActors(@RequestParam(required = false) String firstName,
                                    @RequestParam(required = false) String lastName) {
        if(firstName == null && lastName == null) {
            return this.actorService.getAll();
        } else if (firstName == null) {
            return this.actorService.getAllByLastName(lastName);
        } else if(lastName == null) {
            return this.actorService.getAllByFirstName(firstName);
        } else {
            return this.actorService.getAllByFirstNameAndLastName(firstName, lastName);
        }
    }

    /**
     * Get Function to get an actor based on an id
     * Throws an 'ActorNotFoundException' when the actor with the given id doesn't exist
     * @param id -> The id that you want to search for
     * @return -> The actor with the given id
     */
    @GetMapping("/{id}")
    public Actor getOneActor(@PathVariable Long id) {
        Optional<Actor> response = this.actorService.getOneById(id);
        if(response.isEmpty()) {
            throw new ActorNotFoundException();
        } else {
            return response.get();
        }
    }

    /**
     * Put Function to update an actor
     * Throws an 'IllegalIdChangeException' when you try to change the id of the actor
     * @param id -> The id of the actor that you want to update
     * @param actor -> The actor that you want to update
     * @return -> The updated actor
     */
    @PutMapping("/{id}")
    public Actor updateOneActor(@PathVariable Long id, @RequestBody Actor actor) {
        Optional<Actor> response = this.actorService.getOneById(id);
        if (response.isEmpty()) {
            throw new ActorNotFoundException();
        } else if(!id.equals(actor.getId())) {
            throw new IllegalIdChangeException();
        } else {
            return this.actorService.update(actor);
        }
    }

    /**
     * Post Function to create a new actor
     * Throws an 'IllegalIdChangeException' when you provide an id.
     * @param actor -> Actor that you want to create
     * @return -> The created actor
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Actor addOneActor(@RequestBody Actor actor) {
        if (actor.getId() == null) {
            return this.actorService.create(actor);
        }
        throw new IllegalIdChangeException();
    }

    /**
     * Delete Function to delete an actor based on an id from the database
     * Throws an 'ActorNotFoundException' when the actor with the given id doesn't exist
     * @param id -> The id of the actor that you want to delete
     */
    @DeleteMapping("/{id}")
    public void deleteOneActor(@PathVariable Long id) {
        if(!this.actorService.delete(id)) {
            throw new ActorNotFoundException();
        }
    }
}
