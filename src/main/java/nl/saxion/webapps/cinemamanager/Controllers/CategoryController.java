package nl.saxion.webapps.cinemamanager.Controllers;

import nl.saxion.webapps.cinemamanager.Entities.Category;
import nl.saxion.webapps.cinemamanager.Entities.Movie;
import nl.saxion.webapps.cinemamanager.Exceptions.CategoryNotFoundException;
import nl.saxion.webapps.cinemamanager.Exceptions.IllegalIdChangeException;
import nl.saxion.webapps.cinemamanager.Exceptions.MovieNotFoundException;
import nl.saxion.webapps.cinemamanager.Services.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


/**
 * Created by Thijs van der Vegt (497215)
 * Controller for the Category Entity
 */
@RestController
@RequestMapping("/categories")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CategoryController {

    private final CategoryService categoryService;

    /**
     * Constructor for CategoryController. The Service is needed to handle requests.
     *
     * @param categoryService Spring will use dependency injection to "inject" the categoryService automatically.
     */
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * Get Function to get all the categories
     * @return -> List with all the categories
     */
    @GetMapping
    public List<Category> getAllCategories() {
        return this.categoryService.getAll();
    }

    /**
     * Get Function to get a category based on an id
     * Throws an 'CategoryNotFoundException' when the category with the given id doesn't exist
     * @param id -> The id that you want to search for
     * @return -> Category with the given id
     */
    @GetMapping("/{id}")
    public Category getOneCategory(@PathVariable Long id) {
        Optional<Category> response = this.categoryService.getOneById(id);
        if(response.isEmpty()) {
            throw new CategoryNotFoundException();
        } else {
            return response.get();
        }
    }

    /**
     * Put Function to update a category
     * Throws an 'IllegalIdChangeException' when you try to change the id of the category
     * @param id -> The id of the category that you want to update
     * @param category -> Category object that contains the entire object including the changes
     * @return -> The updated category
     */
    @PutMapping("/{id}")
    public Category updateOneCategory(@PathVariable Long id, @RequestBody Category category) {
        Optional<Category> response = this.categoryService.getOneById(id);
        if (response.isEmpty()) {
            throw new CategoryNotFoundException();
        } else if(!id.equals(category.getId())) {
            throw new IllegalIdChangeException();
        } else {
            return this.categoryService.update(category);
        }
    }

    /**
     * Post Function to create a new category
     * @param category -> The category that you want to create
     * @return -> The created category
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Category addOneCategory(@RequestBody Category category) {
        if (category.getId() == null) {
            return this.categoryService.create(category);
        }
        throw new IllegalIdChangeException();
    }

    /**
     * Delete Function to delete a category based on an id from the database
     * Throws an 'CategoryNotFoundException' when the category with the given id doesn't exist
     * @param id -> The id of the category that you want to delete
     */
    @DeleteMapping("/{id}")
    public void deleteOnCategory(@PathVariable Long id) {
        if(!this.categoryService.delete(id)) {
            throw new CategoryNotFoundException();
        }
    }
}
