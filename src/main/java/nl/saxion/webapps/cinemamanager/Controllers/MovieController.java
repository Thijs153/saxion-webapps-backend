package nl.saxion.webapps.cinemamanager.Controllers;

import nl.saxion.webapps.cinemamanager.Entities.Actor;
import nl.saxion.webapps.cinemamanager.Entities.Movie;
import nl.saxion.webapps.cinemamanager.Exceptions.IllegalIdChangeException;
import nl.saxion.webapps.cinemamanager.Exceptions.MovieNotFoundException;
import nl.saxion.webapps.cinemamanager.Services.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

/**
 * Created by Thijs van der Vegt (497215)
 * Controller for the Movie Entity
 */
@RestController
@RequestMapping("/movies")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MovieController {

    private final MovieService movieService;

    /**
     * Constructor for MovieController. The Service is needed to handle requests.
     *
     * @param movieService Spring will use dependency injection to "inject" the movieService automatically.
     */
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    /**
     * Get Function to get all the movies (optional : based on a title)
     * @param title if given : the title that you want to search for
     * @return -> All movies || All movies with the given title
     */
    @GetMapping
    public List<Movie> getAllMovies(@RequestParam(required = false) String title) {
        return title != null ? this.movieService.getAllByTitle(title) : this.movieService.getAll();
    }

    /**
     * Get Function to get a movie based on an id
     * Throws an 'MovieNotFoundException' when the movie with the given id doesn't exist
     * @param id -> The id that you want to search for
     * @return -> The movie with the given id
     */
   @GetMapping("/{id}")
   public Movie getOneMovie(@PathVariable Long id) {
       Optional<Movie> response = this.movieService.getOneById(id);
       if(response.isEmpty()) {
           throw new MovieNotFoundException();
       } else {
           return response.get();
       }
   }

    /**
     * Get Function to get all the actors from a movie
     * @param id -> The id of a movie that you want to get the actors of
     * @return -> List with all the actors of the movie with the given id
     */
   @GetMapping("/{id}/actors")
   public List<Actor> getAllActorsFromMovie(@PathVariable Long id) {
        Optional<Movie> response = this.movieService.getOneById(id);
        if(response.isEmpty()) {
            throw new MovieNotFoundException();
        } else {
            return response.get().getActors();
        }
   }

    /**
     * Post Function to add an actor to a movie
     * Throws an 'MovieNotFoundException' when the movie with the given id doesn't exist
     * @param id -> The id of the movie
     * @param actor -> The actor that you want to add to the movie
     * @return -> The added actor
     */
   @PostMapping("/{id}/actors")
   public Actor addActorToMovie(@PathVariable Long id, @RequestBody Actor actor) {
        Optional<Movie> response = this.movieService.getOneById(id);
         if(response.isEmpty()) {
            throw new MovieNotFoundException();
         }
        return this.movieService.addOneActorToMovie(response.get(), actor);
    }

    /**
     * Put Function to update a movie
     * Throws an 'IllegalIdChangeException' when you try to change the id of the movie
     * @param id -> The id of the movie that you want to update
     * @param movie -> Movie object that contains the entire object including the changes
     * @return -> The updated movie
     */
   @PutMapping("/{id}")
   public Movie updateOneMovie(@PathVariable Long id, @RequestBody Movie movie) {
        Optional<Movie> response = this.movieService.getOneById(id);
        if (response.isEmpty()) {
            throw new MovieNotFoundException();
        } else if(!id.equals(movie.getId())) {
            throw new IllegalIdChangeException();
        } else {
            return this.movieService.update(movie);
        }
   }

    /**
     * Post Function to create a new movie
     * Throws an 'IllegalIdChangeException' when you provide an id.
     * @param movie -> Movie that you want to create (don't provide an id)
     * @return -> The created movie
     */
   @PostMapping
   @ResponseStatus(HttpStatus.CREATED)
   public Movie addOneMovie(@RequestBody Movie movie) {
        if (movie.getId() == null) {
            return this.movieService.create(movie);
        }
        throw new IllegalIdChangeException();
   }

    /**
     * Delete Function to delete a movie
     * Throws a 'MovieNotFoundException' when the movie with the given id doesn't exist
     * @param id -> The id of the movie you want to delete
     */
   @DeleteMapping("/{id}")
    public void deleteOneMovie(@PathVariable Long id) {
        if(!this.movieService.delete(id)) {
            throw new MovieNotFoundException();
        }
   }
}
